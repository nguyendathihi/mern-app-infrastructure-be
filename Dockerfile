# Giai đoạn 1: Xây dựng ứng dụng
FROM node:13-alpine as builder

WORKDIR /app

COPY ["package.json", "package-lock.json*", "./"]
RUN npm install --production --silent

COPY . .

# Giai đoạn 2: Tạo ảnh nhỏ hơn cho sản xuất
FROM node:13-alpine
WORKDIR /app

COPY --from=builder /app .

USER node
CMD ["node", "app.js"]
